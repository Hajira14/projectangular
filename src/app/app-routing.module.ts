import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { AddprojectComponent } from './addproject/addproject.component';
import { AddtaskComponent } from './addtask/addtask.component';
import { AdduserComponent } from './adduser/adduser.component';

import { ViewtaskComponent } from './viewtask/viewtask.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
const routes: Route[] = [
  { path:'',redirectTo:'/adduser',pathMatch:'full'}, //default url
  {path: 'addproject' , component:AddprojectComponent},
  {path: 'addtask' , component:AddtaskComponent},
  {path: 'adduser' , component:AdduserComponent},
  {path: 'viewtask' , component: ViewtaskComponent},
   {path:'**',component: PagenotfoundComponent} 
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
