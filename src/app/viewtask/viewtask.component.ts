import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/addtask/Task';
import { RouterModule, Router } from '@angular/router';
import { TaskService } from 'src/app/addtask/task.service';
import { ProjectService } from 'src/app/addproject/project.service';
import { UserService } from 'src/app/adduser/user.service';


@Component({
  selector: 'app-viewtask',
  templateUrl: './viewtask.component.html',
  styleUrls: ['./viewtask.component.css']
})
export class ViewtaskComponent implements OnInit {
  public taskDatas=[];
  public searchData=[];
  public tasks;
  public errorMsg;
  public taskModel: Task  =new Task();
  constructor(private router: Router, private _taskService: TaskService) { }
  
  // public disabled: boolean = false;
  
  //  disableAll() {
  //    console.log("HI")
  //    this.disabled = true;
  //  }
  ngOnInit() {
    this.getTasks();
  
  }
  
  getTasks() {
    this._taskService.getTasks()
    .subscribe(data=> this.taskDatas= data,
    error=> this.errorMsg=error);
  console.log(this.taskDatas)
  }



  getParentTasks() {
    this._taskService.getparentTask()
      .subscribe(data => this.taskDatas = data,
      error => this.errorMsg = error);

  }
  updateTask(data){
   this.taskModel =data;
   console.log(this.taskModel)
  }

  endTask(data){
    console.log("HI")
    this._taskService.endTask(data)
    .subscribe(
      response => console.log('Success!', response),
    )
  this.router.navigateByUrl('/pageload', {skipLocationChange: true}).then(()=>
  this.router.navigate(["/viewtask"])); 
  }



  search(term: string) {
    if(!term) {
      this.taskDatas = this.searchData;
    } else {
      this.taskDatas = this.taskDatas.filter(x => 
       //  x.project.projectName.trim().toLowerCase().includes(term.trim().toLowerCase())||
         x.task.trim().toLowerCase().includes(term.trim().toLowerCase())||
         x.priority.toString().trim().toLowerCase().includes(term.trim().toLowerCase()) 
        //  x.parentTask.parenttaskname.trim().toLowerCase().includes(term.trim().toLowerCase())||
        //  x.user.firstName.trim().toLowerCase().includes(term.trim().toLowerCase())
          );
    }
  
  }

}
