import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Task } from './Task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private baseUrl = 'http://localhost:8084/task/tasks';
  private parenturl =  'http://localhost:8084/task/parenttask';
 
  constructor(private http:HttpClient) { }
  addTask(task) {
    console.log("hi");
    return this.http.post(`${this.baseUrl}` + `/create`, task);
}

    

getTasks(): Observable<any>{
  return this.http.get<any>(`${this.baseUrl}`);
}
getparentTask(): Observable<any> {
  console.log("getparenttask")
  return this.http.get<any>(`${this.parenturl}`);
}


updateTask(task: any): Observable<any> {
  return this.http.put(`${this.baseUrl}'+'/update`, task);
}
endTask(task: any): Observable<any> {
  console.log("service end task")
  return this.http.post(`${this.baseUrl}` + `/endtask`, task);
}             


}
