import {Project } from "src/app/addproject/Project";
import { User } from "src/app/adduser/User";
import {ParentTask} from "src/app/addtask/parent-task";
    


export class Task {
    taskId: number;
    project: Project;
    task: string;
    priority: number;
    parenttaskdata: ParentTask;
    startDate: Date;
    endDate: Date;
    user: User;
    isParentTask: boolean;
}
