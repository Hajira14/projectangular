import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/addtask/Task';
import { Router } from '@angular/router';
import { TaskService } from 'src/app/addtask/task.service';
import { ProjectService } from 'src/app/addproject/project.service';
import { UserService } from 'src/app/adduser/user.service';
import { User } from 'src/app/adduser/user';
import { Project } from 'src/app/addproject/project';
import { ParentTask } from 'src/app/addtask/parent-task';

@Component({
  selector: 'app-addtask',
  templateUrl: './addtask.component.html',
  styleUrls: ['./addtask.component.css']
})

export class AddtaskComponent implements OnInit {

  isBoxDisabled;
  public taskDatas=[];


  public errorMsg;
  public data;

  public usersList: any
  public select: User

  public projectsList: any
  public selectpro: Project

  public parentsList: any 
  public selectparent: ParentTask

  public taskModel: Task =new Task();

  constructor(private router: Router, private _projectservice: ProjectService, private _userService: UserService,private _taskService: TaskService) { }


ngOnInit() {
// this.gettasks() 
 this._userService.getusers().subscribe(
      data => this.usersList = data,
      error => this.errorMsg = error);

this._projectservice.getprojects().subscribe(
  data => this.projectsList = data,
  error => this.errorMsg = error);

this._taskService.getparentTask().subscribe (
  data => this.parentsList =data,
  error => this.errorMsg = error);
 }
// onSubmit(data) {
//   console.log(data)
//    this._taskService.addTask(data)
//  .subscribe(
//    response => console.log('Success',response),
//  );
 
 onSubmit(data) {
  
 this.taskModel=data
 console.log("task ts")
    console.log(data)
      this._taskService.addTask(this.taskModel)
       .subscribe(
         response => console.log('Success!', response),
       )
       this.taskModel  =new Task();
       this.router.navigateByUrl('/pagenotfound', {skipLocationChange: true}).then(()=>
       this.router.navigate(["/viewtask"])); 
   }
 


userIsChosen(){
  console.log("success")
  this.taskModel.user=this.select
  // this.obj=this.select[0][0].firstNAme
  $('#user').val(this.select.firstName)
  console.log(this.select.firstName)
}

projectIsChosen(){
  console.log("success")
  this.taskModel.project=this.selectpro
  // this.obj=this.select[0][0].firstNAme
  $('#project').val(this.selectpro.projectName)
  console.log(this.selectpro.projectName)
}


parentIsChosen(){
  console.log("success")
  this.taskModel.parenttaskdata=this.selectparent
  // this.obj=this.select[0][0].firstNAme
  $('#parent').val(this.selectparent.parenttaskname)
  console.log(this.selectparent.parenttaskname)
}

tickparenttask(){
  this.isBoxDisabled =!this.isBoxDisabled;
  return;

}


} 
