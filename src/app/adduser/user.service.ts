import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/adduser/user';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  private baseUrl = 'http://localhost:8084/user/users';

  adduser(user) {
    return this.http.post(`${this.baseUrl}` + `/create`, user);
}
getusers(): Observable<any>{
  return this.http.get<any>(`${this.baseUrl}`);
}
deleteUser(user) {
  console.log('service');
  return this.http.post(`${this.baseUrl}` + `/delete`, user);
}
}
