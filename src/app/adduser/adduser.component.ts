import { Component, OnInit } from '@angular/core';
import {UserService} from "./user.service";
import { User } from 'src/app/adduser/user';
import { RouterModule, Router } from '@angular/router';
import {FormGroup, FormControl} from "@angular/forms";
import {FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

userForm: FormGroup;
userdata = []
errorMsg
filterdata =[]


example = {userId: null, firstName: '', lastName: '',empId: null };

public userModel: User  =new User();
  // userform = new FormGroup({
    //   firstname: new FormControl(''),
    //   lastname: new FormControl(''),
    //   empid: new FormControl(''),
    // });

  constructor(private fb: FormBuilder,private _userService: UserService) { }
  ngOnInit() { 
  this._userService.getusers().subscribe(data => this.filterdata=this.userdata = data,
    error => this.errorMsg = error);
  this.userForm = this.fb.group({
    userId: [],
       firstName: ['', [Validators.required,Validators.minLength(3)]],
       lastName: ['',[Validators.required,Validators.minLength(3)]],
      empId: ['',Validators.required],
   });
  }
   get firstName() {
    return this.userForm.get('firstName');
  }
  get lastName() {
    return this.userForm.get('lastName');
  }
  get empId() {
    return this.userForm.get('empId');
  }
onSubmit() {
console.log(this.userForm.value); 
this._userService.adduser(this.userForm.value)
.subscribe(
  response => console.log('Success',response),
  error => console.log('error',error)
);

}
editUser(data) {
  this.userModel =data;


 this.example = {userId:this.userModel.userId, firstName: this.userModel.firstName, lastName: this.userModel.lastName,empId:this.userModel.empId };

}
deleteUser(data){
  console.log('delete');
  this._userService.deleteUser(data).subscribe(
    response => console.log('Success',response),
    error => console.log('error',error)
  );
}
search(term: string) {
  if(!term) {
    this.userdata = this.filterdata;
  } else {
    this.userdata = this.userdata.filter(x => 
       x.firstName.trim().toLowerCase().includes(term.trim().toLowerCase()) ||
       x.lastName.trim().toLowerCase().includes(term.trim().toLowerCase()) ||
       x.empId.toString().trim().toLowerCase().includes(term.toString().trim().toLowerCase())
    );
  }
}
}

