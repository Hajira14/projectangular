import { User } from "src/app/adduser/user";

export class Project {
    projectId: number;
    projectName: string;
    startDate: Date;
    endDate:Date;
    priority: number;
    user: User;
    noOfTasks: number;
    completedTask: boolean;
    suspendProject: boolean;
}
