import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import * as $ from "jquery";
@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http: HttpClient) { }
  private baseUrl = 'http://localhost:8084/project/projects';

  addproject(project) {
    return this.http.post(`${this.baseUrl}` + `/create`, project);
}
getprojects(): Observable<any>{
  return this.http.get<any>(`${this.baseUrl}`);
}


suspendProject(project: any) : Observable<any> {
  return this.http.post(`${this.baseUrl}` + `/suspend`, project);
}
}
