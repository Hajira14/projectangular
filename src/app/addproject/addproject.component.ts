import { Component, OnInit } from '@angular/core';
import {Project} from "./project";
import {ProjectService} from './project.service';
import {Router} from '@angular/router';
import { UserService } from 'src/app/adduser/user.service';
import { User } from 'src/app/adduser/user';
@Component({
  selector: 'app-addproject',
  templateUrl: './addproject.component.html',
  styleUrls: ['./addproject.component.css']
})
export class AddprojectComponent implements OnInit {
  isDateDisabled=true;

  public projectDatas=[];

  public searchdata =[];
public projects;
  public errorMsg;
  public data;

public projectModel: Project =new Project();
  constructor(private router: Router, private _projectservice: ProjectService, private _userService: UserService) { }

  public usersList: any
  public select: User
  



  selectedfirstname: String
  ngOnInit() {
    this.getproject()

    this._userService.getusers().subscribe(
      data => this.usersList = data,
      error => this.errorMsg = error);

  }

  managerIsChosen(){
    console.log("success")
    this.projectModel.user=this.select
    // this.obj=this.select[0][0].firstNAme
    $('#manager').val(this.select.firstName)
    console.log(this.select.firstName)

   // this.target =this.select.firstName
  //  var obj = {
  //   name: 'myObj'
  // };
  // document.write(this.select.firstName)
    //  var bla = $('#firstName').val();
    //  $("#firstName").val(bla);

  }

  getproject(){
    this._projectservice.getprojects()
    .subscribe(data => this.projectDatas = data,
      error => this.errorMsg = error);
      console.log("this getproject")
      console.log(this.projectDatas)
  }

  dateEditable(){
      this.isDateDisabled =!this.isDateDisabled;
      return;
  }
  
onSubmit(data) {
 console.log(data)
  this._projectservice.addproject(data)
.subscribe(
  response => console.log('Success',response),
);
this.projectModel  =new Project();
this.router.navigateByUrl('/pagenotfound', {skipLocationChange: true}).then(()=>
this.router.navigate(["/addproject"])); 

}

updateProject(project){
  this.projectModel=project;
}

// deleteProject(projectDatas) {
//   console.log(this.projectDatas);
//   this._projectservice.deleteproject(this.projectDatas)
//   .subscribe(
//     response => console.log('Success',response),

    
//   );
// }

search(term: string) {
  if(!term) {
    this.projectDatas = this.searchdata;
  } else {
    this.projectDatas = this.projectDatas.filter(x => 
       x.projectName.trim().toLowerCase().includes(term.trim().toLowerCase())||
       x.priority.toString().trim().toLowerCase().includes(term.trim().toLowerCase())||
       x.user.firstName.trim().toLowerCase().includes(term.trim().toLowerCase())
       );
  }

}


suspendProject(data){
  this._projectservice.suspendProject(data)
  .toPromise()
  .then(() => {
    this.getproject();
    this.router.navigateByUrl('/pageload', { skipLocationChange: true }).then(() =>
    this.router.navigate(["/addproject"]));
  })
  
}




}





